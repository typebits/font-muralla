# Muralla

A sturdy typeface which won't take no for an answer.

Made in the [Type:Bits workshop](https://typebits.gitlab.io) in Avilés, 2017.

![Font preview](https://gitlab.com/typebits/font-muralla/-/jobs/artifacts/master/raw/Muralla-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-muralla/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-muralla/-/jobs/artifacts/master/raw/Muralla-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-muralla/-/jobs/artifacts/master/raw/Muralla-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-muralla/-/jobs/artifacts/master/raw/Muralla-Regular.sfd?job=build-font)

# Authors

This typeface was created by ESAPA students at the Type:Bits workshop in Avilés, 2017.

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
